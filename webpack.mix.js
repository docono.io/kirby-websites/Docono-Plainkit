let mix = require('laravel-mix');

// Globals
let projectDomain = 'ts-architektur.test/';
mix.setPublicPath('assets');
// Directories to Copy
mix.copy('development/fonts', 'assets/fonts');
mix.copy('development/img', 'assets/img');
mix.copy('development/icons', 'assets/icons');
// JS
mix.js('development/js/website.js', 'js').version();
// SCSS
mix.sass('development/scss/website.scss', 'css');


// Browsersync
mix.browserSync({
    proxy: projectDomain,
    ghostMode: false
});
